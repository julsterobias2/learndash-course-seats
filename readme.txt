=== Plugin Name ===
Contributors: binigwasay
Donate link: http://julsterobias.github.com/me
Tags: course, maximum of seats
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Provide LearnDash number of seats per course

== Description ==

Will limit enrollee by providing maximum number of seats per course. Once the maximum number of seats is reach the course will automatically close from enrollment.


== Installation ==

1. Upload `learndash-course-seats.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== How to use? ==
1. Add/Edit Course
2. Go to settings of the course
3. Under course access settings you should find an option called "Maximum Seats"
4. Add the number of desired seats you want
5. Save

== Frequently Asked Questions ==

= Can I add students in the admin even the maximum number of seats reached? =
Yes

= If I change my mind how can I remove the maximum seats? =
Just set the maximum seats to 0 in the admin.

= What happened if the maximum seats are reached? =
The course will automatically closed from enrollment


== Screenshots ==

1. assets/screenshot1.jpg
2. assets/screenshot2.jpg

== Changelog ==

= 1.0 =
* Initial Release


== Features ==

1. Easy to use 
2. Admin UI is feels like part of LearnDash
3. No complicated setup just plug and play