<?php 
/*
 * Plugin Name: LearnDash Course Seats
 * Description: Course enrollment seats
 * Author:      Juls Terobias
 * Plugin Type: Functionality
 * Version: 1.0
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


define('LDCS_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
define('LDCS_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('LDCS_PLUGIN_FILE', dirname( __FILE__ )  );

function ldcs_install(){
	if(class_exists('SFWD_LMS'))
		return;

	echo '<h3>'.__('LearnDash plugin is required', 'ldcs').'</h3>';
    @trigger_error(__('Please install LearnDash', 'ldcs'), E_USER_ERROR);
}
register_activation_hook( __FILE__, 'ldcs_install' );


spl_autoload_register(function ($class) {
	if(strpos($class,'ldcs') !== false){
		$class = preg_replace('/\\\\/', '{ldcs}', $class);
        $class = explode('{ldcs}', $class);
		if(@$class[1]){
            $class_name = str_replace('_','-',$class[1]);
            if(strpos($class_name,'admin') !== false){
            	include_once 'admin/'.$class_name.'.php';
            }else{
            	include_once 'includes/'.$class_name.'.php';
            }
		}
	}
});

add_action('plugins_loaded','ldcs_init_plugin');
function ldcs_init_plugin(){
	new ldcs_main\ldcs_main_class();
	new ldcs_admin\ldcs_admin_class();
}
?>