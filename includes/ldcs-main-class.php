<?php
namespace ldcs_main;
use ldcs_course;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ldcs_main_class{
	function __construct(){
		new ldcs_course\ldcs_course_class();
		add_action('wp_enqueue_scripts', array($this,'assets'));
	}

	function assets(){
		if(is_admin())
			return;

		wp_enqueue_style('dsx-chat-stlye',LDCS_PLUGIN_URL.'assets/style.css');
	}


}