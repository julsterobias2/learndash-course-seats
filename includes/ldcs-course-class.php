<?php
namespace ldcs_course;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ldcs_course_class{
	
	var $course_id = 0;
	var $remaining_seats = 0;
	var $course_seats = 0;

	function __construct(){
		add_action('template_redirect',array($this,'on_page'));
	}

	function on_page(){
		if(get_post_type() == 'sfwd-courses'){
			global $post;

			$this->course_id = $post->ID;
			$course_seats = learndash_get_course_meta_setting($post->ID);

			if($course_seats['sfwd-courses_seats_number'] == 0)
				return;

			if($this->is_enrolled())
				return;

			$users = get_users(
				array(
			    'meta_key' => 'course_'.$this->course_id.'_access_from',
				)
			);

			$remaining = $this->seat_remaining($course_seats,count($users));
			if($remaining == 0){
				$this->update_pricing($post->ID);
			}
			add_filter('the_content',array($this,'content'));
		}
	}

	function update_pricing($post_id = 0){
		if($post_id == 0)
			return;

		$get_course_meta = learndash_get_course_meta_setting($post_id);
		if($get_course_meta['sfwd-courses_course_price_type'] != 'closed'){
			$get_course_meta['sfwd-courses_course_price_type'] = 'closed';
			update_post_meta($post_id,'_sfwd-courses',$get_course_meta);
		}
	}

	function seat_remaining($max_seats = 0, $users_count = 0){
		$remaining = $max_seats['sfwd-courses_seats_number'] - $users_count;
		$this->remaining_seats = $remaining;
		return $remaining;
	}

	function is_enrolled(){

		$id = get_current_user_id();
		if(!$id)
			return;

		return get_user_meta($id,'course_'.$this->course_id.'_access_from',true);
	}

	function content($content){
		ob_start();
		$this->set_template('enrolled-count.php');
		$template_content = ob_get_contents();
		ob_end_clean();

		$filteredcontent = $template_content . $content;
    	return $filteredcontent;
	}

	function set_template($file,$params = null){
		$path = get_template_directory().'/learndash-course-seats/templates';
		$child = get_template_directory().'-child/learndash-course-seats/templates';
		if(is_dir($path)){
			include_once $path.'/'.$file;
		}elseif(is_dir($child)){
			include_once $child.'/'.$file;
		}else{
			include_once LDCS_PLUGIN_FILE.'/templates/'.$file;
		}
	}

}