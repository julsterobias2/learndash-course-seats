<?php
/**
 * Functions for uninstall LearnDash
 *
 * @since 2.5.0
 *
 * @package LearnDash Course Seats
 */

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}