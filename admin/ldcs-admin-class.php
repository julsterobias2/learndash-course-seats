<?php
namespace ldcs_admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ldcs_admin_class{

	var $post_type = 'sfwd-courses';

	function __construct(){
		add_filter('learndash_settings_fields',array($this,'seat_settings'),10 ,2);	
		add_filter('learndash_settings_save_values',array($this,'save_meta'),10,2);
	}

	function seat_settings($option_fields,$meta_box){
		foreach($option_fields as $index => $val){
			if($index == 'expire_access_days'){
				$get_seats = $this->seats_num();
				$option_fields['seats_number'] = array(
					'name' => 'seats_number',
					'label' => 'Maximum of seats',
					'type' => 'number',
					'class' => 'small-text',
					'min' => 0,
					'value' => $get_seats,
					'input_label' => 'No. of Seats',
					'help_text' => 'Number of students allowed to enroll this course'
				);
			}
		}
		return $option_fields;
	}

	function seats_num(){
		if(isset($_GET['post'])){
			$data = learndash_get_course_meta_setting($_GET['post']);
			return ($data[$this->post_type.'_seats_number'] > 0)? $data[$this->post_type.'_seats_number'] : 0;
		}else{
			return 0;
		}
	}

	function save_meta($settings_values, $settings_section_key){
		$settings_values['seats_number'] = $_POST['learndash-course-access-settings']['seats_number'];
		return $settings_values;
	}


}